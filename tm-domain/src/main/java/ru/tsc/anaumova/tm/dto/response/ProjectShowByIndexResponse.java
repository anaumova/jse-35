package ru.tsc.anaumova.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.anaumova.tm.model.Project;

@NoArgsConstructor
public final class ProjectShowByIndexResponse extends AbstractProjectResponse {

    public ProjectShowByIndexResponse(@Nullable final Project project) {
        super(project);
    }

}
